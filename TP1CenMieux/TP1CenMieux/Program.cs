﻿using System;

namespace TP1CenMieux
{
    class Program
    {
        static void Main(string[] args)
        {   
            //Print every multiplication tablefrom 1*1 to 10*10.
            //Each line is a table 
            tabDeUnADix();

            // Print only the odd result (i.e.: 3*7 = 21). Tip: use the modulo (%) operator.
            tabDeUnADixImpair();

            multiplicationTableUserParameter();

            Prime();

            Fibonacci();

            Factorial();
            
            TryCatch();
            
            rectangle();

            RectangleWithStars();

            tree();

        }

        //Print every multiplication tablefrom 1*1 to 10*10.
        //Each line is a table 
        private static void tabDeUnADix()
        {
            Console.WriteLine("Print every multiplication tablefrom 1*1 to 10*10.");
            int i = 1;
            while (i <= 10)
            {
                for (int j = 1; j <= 10; j++)
                {
                    Console.Write((j * i) + " ");

                }
                Console.WriteLine();
                i++;
            }
            Console.ReadKey();
        }

        // Print only the odd result (i.e.: 3*7 = 21). Tip: use the modulo (%) operator.
        private static void tabDeUnADixImpair()
        {
            Console.WriteLine();
            Console.WriteLine("Print only the odd result ");
            int i = 1;
            while (i <= 10)
            {
                for (int j = 1; j <= 10; j++)
                {
                    if (((i * j) % 2) == 1)
                    {
                        Console.Write((j * i) + " ");
                    }
                }
                Console.WriteLine();
                i++;
            }
            Console.ReadKey();
        }
        
        private static void multiplicationTableUserParameter()
        {
            int i, number;
            Console.Write("Input the number : ");
            number = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n");
            for (i = 1; i <= 10; i++)
            {
                Console.Write("{0} X {1} = {2} \n", number, i, number * i);
            }
        }
        static bool isPrime(int number)
        {
            //since 0 and 1 is not prime return false.
            if (number == 1 || number == 0) return false;

            //Run a loop from 2 to n-1
            for (int i = 2; i < number; i++)
            {
                // if the number is divisible by i, then n is not a prime number.
                if (number % i == 0) return false;
            }
            //otherwise, n is prime number.
            return true;
        }

        private static void Prime()
        {
            Console.WriteLine();
            Console.WriteLine("Prime number between 1 and 1000");
            Console.WriteLine();

            int Max = 1000;
            //check for every number from 1 to N
            for (int i = 1; i <= Max; i++)
            {
                //check if current number is prime
                if (isPrime(i))
                {
                    Console.Write(i + " ");
                }
            }
            Console.WriteLine();
        }
        private static void Fibonacci()
        {
            Console.WriteLine();
            long nbOne = 0, nbTwo = 1, nbThree, i, number;
            Console.Write("Enter the number of elements for the fibonacci suit you want: ");
            number = Convert.ToInt32(Console.ReadLine());

            if (number == 0)
            {
                Console.WriteLine(nbOne);
            }
            else if (number == 1)
            {
                Console.WriteLine(nbTwo);
            }
            else
            {
                Console.Write(nbOne + " " + nbTwo + " "); //printing 0 and 1    
                for (i = 2; i < number; ++i) //loop starts from 2 because 0 and 1 are already printed    
                {
                    nbThree = nbOne + nbTwo;
                    Console.Write(nbThree + " ");
                    nbOne = nbTwo;
                    nbTwo = nbThree;
                }
            }
            Console.WriteLine();
        }
        private static void Factorial()
        {
            Console.WriteLine();
            int i, factorial = 1, number;
            Console.Write("Enter any Number to calculate is factorial: ");
            number = int.Parse(Console.ReadLine());
            for (i = 1; i <= number; i++)
            {
                factorial = factorial * i;
            }
            Console.Write("Factorial of " + number + " is: " + factorial);
        }
        /*
        What would happen if you tried to calculate 420 000! ?
        The size of an int is 32 bit so if I try to calculate 420000! the result is 0. This overloads the int so ca does not compute properly. 
        The maximum factor I can calculate with my function is factor 12!

        What is recursive function ?
        A recursive program/ recursive function is a programming technique that replaces loop instructions with function calls. The mechanism therefore generally consists in creating a function which is called itself once or several times according to different criteria.
        */
        private static void TryCatch()
        {
            Console.WriteLine();
            Console.WriteLine("Try Catch Function");
            for (int i = -3; i < 4; i++)
            {
                Console.WriteLine();
                
                try
                {
                    //y = 10 / (x²-4)
                    int y = 10/ (int)(Math.Pow(i, 2) - 4);
                    Console.WriteLine(y);

                }
                catch (DivideByZeroException ex)
                {
                    Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
                    // Action after the exception is caught  
                }
            }
           

        }
        
        public static void rectangle()
        {
            Console.Write("enter a rectangle width between 1 and 1000: ");
            int width;
            width = int.Parse(Console.ReadLine());

            Console.Write("enter a rectangle length between 1 and 1000 : ");
            int length;
            length = int.Parse(Console.ReadLine());

            string rectangle = "";

            if (width <= 1000 && width >= 1 && length <= 1000 && length >= 1)
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < length; j++)
                    {
                        if ((i == 0 && j == 0) || (i == width - 1 && j == 0) || (i == 0 && j == length - 1) || (i == width - 1 && j == length - 1))
                        {
                            rectangle += "o";
                        }
                        else if ((j == 0) || (j == length - 1))
                        {
                            rectangle += "|";
                        }
                        else if ((i == 0) || (i == width - 1))
                        {
                            rectangle += "-";
                        }
                        else
                        {
                            rectangle += " ";
                        }
                    }
                    rectangle += "\n";
                }
                Console.WriteLine(rectangle);
            }
            else
            {
                Console.WriteLine("Les dimentions ne sont pas respectées");
            }
            
        }

        public static void RectangleWithStars()
        {
            Console.Write("enter a rectangle width between 1 and 1000: ");
            int width;
            width = int.Parse(Console.ReadLine());

            Console.Write("enter a rectangle length between 1 and 1000 : ");
            int length;
            length = int.Parse(Console.ReadLine());

            string rectangle = "";
            if (width <= 1000 && width >= 1 && length <= 1000 && length >= 1)
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < length; j++)
                    {
                        var star = j % 3;
                        if (i == 0 && j == 0 || i == width - 1 && j == 0 || i == 0 && j == length - 1 || i == width - 1 && j == length - 1)
                        {
                            rectangle += "o";
                        }
                        else if (j == 0 || j == length - 1)
                        {
                            rectangle += "|";
                        }
                        else if (i == 0 || i == width - 1)
                        {
                            rectangle += "-";
                        }
                        else if (i % 3 == star)
                        {
                            rectangle += "*";
                        }
                        else
                        {
                            rectangle += " ";
                        }
                    }
                    rectangle += "\n";
                }
                Console.WriteLine(rectangle);
            }
            
        }

        private static void tree()
        {
            Console.WriteLine("Saisisez un nombre entre 3 et 20 ");
            int n = int.Parse(Console.ReadLine());
            if ( n < 21 && n > 2)
            {
                for (int i = 0; i <= n - 1; i++)
                {
                    var stars = new string('*', i);
                    var spaces = new string(' ', n - i);
                    Console.Write(spaces);
                    Console.Write(stars);
                    Console.Write("*");
                    Console.Write(stars);
                    Console.WriteLine(spaces);
                }

                var spaces_tree = new string(' ', n - 1);
                Console.Write(spaces_tree);
                var pied = new string("| |");
                Console.Write(pied);
            }
            else
            {
                Console.WriteLine("Le chiffre saisie n'est pas entre 3 et 20");
            }
            
        }
    }
}
